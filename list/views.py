from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
import os
import sys


def index(request, begin=0, end=50, folder=""):
    begin = int(begin)
    end = int(end)
    path = os.path.join(settings.BASE_DIR, "list/static/list/", folder)
    files = sorted(os.listdir(path))[begin:end]

    bad_path = os.path.join(path, "sorted")
    if not os.path.exists(bad_path):
        os.makedirs(bad_path)

    path = os.path.join(settings.BASE_DIR, "list/static/list/",
                        folder, "sorted")
    score = len(os.listdir(path))

    pics = ["list/{}/{}".format(folder, file) for file in files]
    context = {'lists': sorted(pics), 'begin': begin, 'end': end,
               'score': score, 'folder': folder}
    return render(request, 'list/index.html', context)


def move(request, begin, end, folder):
    path = os.path.join(settings.BASE_DIR, "list/static/list/")

    for file in request.POST.getlist('files'):
        filename = file.split("/")[-1]
        os.rename(os.path.join(path, folder, filename),
                  os.path.join(path, folder, "sorted", filename))
    size = int(end) - int(begin)
    return HttpResponseRedirect(reverse('list:index',
                                        kwargs={'begin': end,
                                                'end': int(end) + size,
                                                'folder': folder}
                                        ))
