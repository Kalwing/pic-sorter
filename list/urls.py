from django.urls import path

from . import views

app_name = 'list'
urlpatterns = [
    path('<str:folder>', views.index, name='index'),
    path('<str:folder>/<int:begin>/<int:end>', views.index, name='index'),
    path('move/<str:folder>/<int:begin>/<int:end>', views.move, name='move')
]
