import numpy as np
import cv2
import os
from aux import initDir

SOURCE_BOTTOM = "../lines/static/lines/raw_lines/Handwritten"
SOURCE_TOP = "../lines/static/lines/raw_lines/Typescript"
# Put the merged image directly in the image sorter. Access it at:
# http://127.0.0.1:8000/list/merged/0/100/
DEST = "../list/static/list/merged"

# Arbitrary number to select image to assemble by their sizes
RES_OFFSET = 5000

initDir(SOURCE_BOTTOM, must_exist=True, can_be_empty=False)
initDir(SOURCE_TOP, must_exist=True, can_be_empty=False)
initDir(DEST)

# List the path of all the files in the top and bottom folders
bfiles = [os.path.join(SOURCE_BOTTOM, file)
          for file in os.listdir(SOURCE_BOTTOM)]
tfiles = [os.path.join(SOURCE_TOP, file)
          for file in os.listdir(SOURCE_TOP)]

# Make a list of tuple as such : ("file/path.jpg", filesize). Sorted by their
# size in bytes
bfiles_sizes = [(file, os.path.getsize(file)) for file in bfiles]
bfiles_sizes = sorted(bfiles_sizes, key=lambda t: t[1])
tfiles_sizes = [(file, os.path.getsize(file)) for file in tfiles]
tfiles_sizes = sorted(tfiles_sizes, key=lambda t: t[1])

nb = 0
for i, bfile in enumerate(bfiles_sizes):
    tfile = tfiles_sizes[i]
    # For the pictures having "close enough" resolutions
    if tfile[1] - RES_OFFSET < bfile[1] < tfile[1] + RES_OFFSET:
        bot = cv2.imread(bfile[0], 1)
        top = cv2.imread(tfile[0], 1)
        # If they have the same horizontal size
        if len(bot[0]) == len(top[0]):
            # create an image consisting of "top" on top of "bot"
            img = np.concatenate((top[len(top)//2:], bot), axis=0)
            # Save the file in DEST
            name = bfile[0].split("/")[-1][:-4]
            path = os.path.join(DEST, "merged{}.jpg".format(name))
            cv2.imwrite(path, img)
            nb += 1

print("{} files were written in {}".format(nb, DEST))
