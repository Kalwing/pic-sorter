import os


def initDir(path: str, must_exist: bool = False, can_be_empty: bool = True):
    """
    Check if path exists. If it doesn't and must_exist isn't True,
    make the directory.
    Args:
        path: a string representing a path to the directory.
        must_exist: a Boolean telling if the directory pointed by path must
                    exist before calling the function
        can_be_empty: A boolean telling if the directory can be empty
    """
    if not os.path.exists(path):
        if must_exist:
            raise FileNotFoundError("Directory doesn't exist")
        elif can_be_empty:
            os.makedirs(path)
        else:
            raise FileNotFoundError("Directory doesn't exist, \
                                    but can't be empty")
    if not can_be_empty and len(os.listdir(path)) == 0:
        raise FileNotFoundError("Directory is empty")
