from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings
import os

FOLDERS = {'1': "Handwritten",
           '2': "Typescript",
           '3': "Bad",
           '4': "Multiples_Lines"}


def index(request, begin=0, end=50, folder=""):
    begin = int(begin)
    end = int(end)
    base_path = os.path.join(settings.BASE_DIR, "lines/static/lines/", folder)

    # list all the files alphabetically and excludes folders
    files = sorted(os.listdir(base_path))
    files = [file for file in files
             if os.path.isfile(os.path.join(base_path, file))]

    # Only keep the files in a certain range
    end = min(end, len(files))
    files = files[begin:end]

    folders_path = []
    score = {}
    # Create the folder that will store the sorted files (if they do not exist)
    # and count the files in them
    for key in FOLDERS.keys():
        folder_path = os.path.join(base_path, FOLDERS[key])
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        folders_path.append(os.path.join(folder_path))
        score[FOLDERS[key]] = len(os.listdir(folder_path))
    # Convert scores to percentages
    score['Total'] = sum(score.values())
    for key in score.keys():
        if key != 'Total':
            score[key] = "{:.3}".format((score[key] / score['Total']) * 100)
    # Sort the dict to keep the button in the same order
    sort_folders = [(key, FOLDERS[key]) for key in FOLDERS.keys()]
    sort_folders = sorted(sort_folders, key=lambda t: t[0])


    pics = ["lines/{}/{}".format(folder, file) for file in files]
    context = {'lists': sorted(pics), 'begin': begin, 'end': end,
               'score': score, 'folder': folder, 'sort_folders': sort_folders}
    return render(request, 'lines/index.html', context)


def move(request, begin, end, folder):
    path = os.path.join(settings.BASE_DIR, "lines/static/lines/", folder)

    # Get all files selected
    items = [item for item in request.POST.items()
             if item[1] in FOLDERS.keys()]
    # item is formatted like that :
    # (filename, key)
    for item in items:
        print(item[0])
        filename = item[0].split("/")[-1]
        os.rename(os.path.join(path, filename),
                  os.path.join(path, FOLDERS[item[1]], filename))
    return HttpResponseRedirect(reverse('lines:index',
                                        kwargs={'begin': begin,
                                                'end': end,
                                                'folder': folder}
                                        ))
